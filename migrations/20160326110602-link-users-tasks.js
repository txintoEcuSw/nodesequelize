'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.addColumn(
      'Tasks',
      'UserId',
      {
        type: Sequelize.INTEGER,
        references: { 
          model: 'Users', key: 'id' 
        }
      }
    ).then(function(){
        queryInterface.addColumn(
          'Tasks',
          'complete',
          Sequelize.BOOLEAN
        )
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.removeColumn('Task','complete').then(function(){
        queryInterface.removeColumn('Task','UserId')
        }
      );
    }

};
